/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modèle;

import service.ModelView;
import modèle.AnnotationURL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author NAINA
 */
public class Departement {
    int id;
    String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @AnnotationURL(url="liste_Departement.do")
    public static ModelView liste(){
    System.out.println("Tonga ehh");
    HashMap<String, Object>Hval=new HashMap<>();
    Departement[] all=Departement.get_all();
    ModelView md=new ModelView();
        Hval.put("liste_Departement", all);
        md.setData(Hval);
        md.setUrl("liste_Departement.jsp");
    return md;
    }
    public static Departement[] get_all()
    {
        Departement[] valiny=new Departement[3];
        valiny[0]=new Departement(1,"amphi A");
        valiny[1]=new Departement(2, "amphi C");
        valiny[2]=new Departement(3, "amphi B");
        
        return valiny;
    }

    public Departement() {
    }
    

    public  Departement(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
}
